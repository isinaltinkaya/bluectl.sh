# bluectl.sh

A script to automate the bluetooth connection with your device via bluetoothctl.

# Dependencies:

You need bluetoothctl and expect installed.

# Usage:

First you need to pair with your bluetooth device following this documentation: https://docs.ubuntu.com/core/en/stacks/bluetooth/bluez/docs/reference/pairing/introduction

Then give the script proper permissions with ' chmod +x bluectl.sh '

"./bluectl.sh YOUR_DEVICE_ID " will connect you with your device. Your device ID should be written in this format: 70:00:1C:0E:66:97

You can make an alias for this script. Just add these lines to your .bashrc or zshrc:

alias YOUR_PREFERRED_WORD='/PATH_TO_SCRIPT/bluectl.sh YOUR_DEVICE_ID'

In my case I use this script for my JBL bluetooth speaker and I use the alias like this:

alias jbl='/PATH/bluectl.sh JBL'S_DEVICE_ID'