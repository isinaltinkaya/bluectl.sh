#!/usr/bin/expect -f

set prompt "#"
set address [lindex $argv 0]

spawn bluetoothctl
expect -re $prompt
send "power on\r"
expect "Changing power on succeeded"
send "connect $address\r"
expect {
    "Connection successful" { send -- "quit\r" }
    "Failed to connect: org.bluez.Error.Failed" { send -- "connect $address\r" }
}
expect eof